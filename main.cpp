/*
	2D numerical simulation physics kernel with user-defined rockets for apogee and range design.

	Author : Richard Kwok
	Date : 2018/08/18
*/

#include <memory>
#include <string>
#include "testing.h"
#include "vector.h"

// https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}


class Rocket;

class RocketState
{
public:
	virtual double getHeadingDragCoefficient(Rocket& rocket) = 0;
	virtual double getHeadingArea(Rocket& rocket) = 0;
};

class Rocket
{

public:

	Rocket(	double nose_cone_drag_coefficient, double parachute_drag_coefficient, double fin_drag_coefficient,
			double nose_cone_area, double parachute_area, double fin_area, double cop_offset,
			double average_thrust, double burn_time, double recovery_delay, double mass, double inertia)

		: nose_cone_drag_coefficient(nose_cone_drag_coefficient)
		, parachute_drag_coefficient(parachute_drag_coefficient)
		, fin_drag_coefficient(fin_drag_coefficient)
		, nose_cone_area(nose_cone_area)
		, parachute_area(parachute_area)
		, fin_area(fin_area)
		, cop_offset(cop_offset)
		, average_thrust(average_thrust)
		, burn_time(burn_time)
		, recovery_delay(recovery_delay)
		, mass(mass)
		, inertia(inertia)
	{

	}

	// Methods supporting the variable drag state machine
	template <typename T>
	void setState()
	{
		this->state = std::unique_ptr<T>(new T());
	}

	double getNoseConeDragCoefficient(void) const
	{
		return this->nose_cone_drag_coefficient;
	}

	double getParachuteDragCoefficient(void) const
	{
		return this->parachute_drag_coefficient;
	}

	double getNoseConeArea(void) const
	{
		return this->nose_cone_area;
	}

	double getParachuteArea(void) const
	{
		return this->parachute_area;
	}

	void incrementTimeSinceIgnition(double dt)
	{
		this->time_since_ignition += dt; // dt in seconds
	}

	double getTimeSinceIgnition(void)
	{
		return this->time_since_ignition;
	}

	double getRecoveryTime(void)
	{
		return this->burn_time + this->recovery_delay;
	}

	// Methods used by the physics kernel
	Vector2D getHeading(void)
	{
		return Vector2D(-sin(angle), cos(angle));
	}

	Vector2D getThrust(void)
	{
		if (this->time_since_ignition <= this->burn_time)
		{
			return this->getHeading() * this->average_thrust;
		}
		return Vector2D(0.0, 0.0);
	}

	double getHeadingDragCoefficient(void)
	{
		return this->state->getHeadingDragCoefficient(*this);
	}

	double getFinDragCoefficient(void) const
	{
		return this->fin_drag_coefficient;
	}

	double getMass(void) const
	{
		return this->mass;
	}

	double getInertia(void) const
	{
		return this->inertia;
	}

	double getHeadingArea(void)
	{
		return this->state->getHeadingArea(*this);
	}

	double getFinArea(void) const
	{
		return this->fin_area;
	}

	double getCopOffset(void) const
	{
		return this->cop_offset;
	}

	Vector2D 	position; // metres between origin (bottom of launch rail) and rocket COG
	Vector2D 	velocity; // metres/s
	double 		angle; // counterclockwise from vertical in radians
	double 		angular_velocity; // radians/s

private:
	std::unique_ptr<RocketState>	state;
	double 							time_since_ignition;

	const double	nose_cone_drag_coefficient;
	const double 	parachute_drag_coefficient;
	const double	fin_drag_coefficient;
	const double	nose_cone_area;
	const double	parachute_area;
	const double	fin_area;
	const double	cop_offset;
	const double 	average_thrust; // N
	const double 	burn_time; // seconds
	const double 	recovery_delay; // seconds
	const double 	mass;
	const double 	inertia;

};

class RecoveryRocketState: public RocketState
{
public:
	double getHeadingDragCoefficient(Rocket& rocket) override
	{
		return rocket.getParachuteDragCoefficient();
	}

	double getHeadingArea(Rocket& rocket) override
	{
		return rocket.getParachuteArea();
	}
};

class FlightRocketState: public RocketState
{
public:
	double getHeadingDragCoefficient(Rocket& rocket) override
	{
		return rocket.getNoseConeDragCoefficient();
	}

	double getHeadingArea(Rocket& rocket) override
	{
		return rocket.getNoseConeArea();
	}
};

class RocketBuilder
{
public:
	Rocket create(void) const
	{
		return Rocket(this->nose_cone_drag_coefficient, this->parachute_drag_coefficient, this->fin_drag_coefficient,
						this->nose_cone_area, this->parachute_area, this->fin_area, this->cop_offset,
						this->average_thrust, this->burn_time, this->recovery_delay, this->mass, this->getInertia());
	}

	void setNoseConeDragCoefficient(double nose_cone_drag_coefficient)
	{
		this->nose_cone_drag_coefficient = nose_cone_drag_coefficient;
	}

	void setParachuteDragCoefficient(double parachute_drag_coefficient)
	{
		this->parachute_drag_coefficient = parachute_drag_coefficient;
	}

	void setFinDragCoefficient(double fin_drag_coefficient)
	{
		this->fin_drag_coefficient = fin_drag_coefficient;
	}

	void setNoseConeArea(double nose_cone_area)
	{
		this->nose_cone_area = nose_cone_area;
	}

	void setParachuteArea(double parachute_area)
	{
		this->parachute_area = parachute_area;
	}

	void setFinArea(double fin_area)
	{
		this->fin_area = fin_area;
	}

	void setCopOffset(double cop_offset)
	{
		this->cop_offset = cop_offset;
	}

	void setAverageThrust(double average_thrust)
	{
		this->average_thrust = average_thrust;
	}

	void setBurnTime(double burn_time)
	{
		this->burn_time = burn_time;
	}

	void setRecoveryDelay(double recovery_delay)
	{
		this->recovery_delay = recovery_delay;
	}

	void setRadius(double radius)
	{
		this->radius = radius;
	}

	void setLength(double length)
	{
		this->length = length;
	}

	void setMass(double mass)
	{
		this->mass = mass;
	}

private:
	double getInertia(void) const
	{
		// Approximate as cylinder
		return 0.25 * this->mass * this->radius * this->radius + 0.0833 * this->mass * this->length * this->length;
	}

	double nose_cone_drag_coefficient;
	double parachute_drag_coefficient;
	double fin_drag_coefficient;
	double nose_cone_area;
	double parachute_area;
	double fin_area;
	double cop_offset;
	double average_thrust;
	double burn_time;
	double recovery_delay;
	double radius;
	double length;
	double mass;
};

class PhysicsEnvironment;

class PhysicsState
{
public:
	virtual void onEntry(PhysicsEnvironment& env) = 0;
	virtual void onStepSimulation(PhysicsEnvironment& env) = 0;
};

class PhysicsEnvironment
{
public:
	PhysicsEnvironment(const RocketBuilder& builder, Vector2D launch_rail, Vector2D wind_velocity)
		: rocket(builder.create())
		, launch_rail(launch_rail)
		, wind_velocity(wind_velocity)
	{

	}

	template <typename T>
	void setState(void)
	{
		this->state = std::unique_ptr<T>(new T());
		this->state->onEntry(*this);
	}

	void stepSimulation(void)
	{
		std::cout << rocket.position << std::endl;
		// if the time exceeds the end of the burn + the recovery delay, trigger recovery state
		if (rocket.getTimeSinceIgnition() > rocket.getRecoveryTime())
		{
			rocket.setState<RecoveryRocketState>();
		}
		this->state->onStepSimulation(*this);

	}

	void simulate(void)
	{
		// launch in separate thread for efficiency
		rocket.setState<FlightRocketState>();
		while (!(this->complete))
		{
			this->stepSimulation();
		}
	}

	void setComplete(void)
	{
		this->complete = true;
	}

	Vector2D getLaunchRail(void) const
	{
		return this->launch_rail;
	}

	Vector2D getGravitationalAcceleration(void) const
	{
		return this->gravitational_acceleration;
	}

	Vector2D getWindVelocity(void) const
	{
		return this->wind_velocity;
	}

	double getAirDensity(void) const
	{
		return this->air_density;
	}

	double getSimulationStepTimeInSeconds(void)
	{
		return this->step_time_milliseconds * 1e-3;
	}

	Rocket rocket;

private:
	std::unique_ptr<PhysicsState> state;
	bool complete = false;

	const Vector2D launch_rail; // m
	const Vector2D gravitational_acceleration = Vector2D(0.0, -9.81); // m/s^2
	const Vector2D wind_velocity; // m/s
	const double air_density = 1.225; // kg/m^3
	const double step_time_milliseconds = 1.0; // should be user configurable
};

class LandedPhysicsState: public PhysicsState
{
public:

	void onEntry(PhysicsEnvironment& env) override
	{
		env.setComplete();
	}

	void onStepSimulation(PhysicsEnvironment& env) override
	{
		// do nothing
	}
};

class FlightPhysicsState: public PhysicsState
{
public:
	void onEntry(PhysicsEnvironment& env) override
	{
		// do nothing
	}

	void onStepSimulation(PhysicsEnvironment& env) override
	{
		Vector2D flow_velocity = env.getWindVelocity() - env.rocket.velocity;

		double lift_speed = flow_velocity.dot(env.rocket.getHeading().getRhsOrthogonalVector());
		Vector2D lift = env.rocket.getHeading().getRhsOrthogonalVector() * env.rocket.getFinDragCoefficient() * env.getAirDensity() *
						0.5 * lift_speed * lift_speed * env.rocket.getFinArea();

		double drag_speed = flow_velocity.dot(env.rocket.getHeading());
		Vector2D drag = env.rocket.getHeading() * env.rocket.getHeadingDragCoefficient() * env.getAirDensity() *
						0.5 * drag_speed * drag_speed * env.rocket.getHeadingArea();

		Vector2D force = (env.getGravitationalAcceleration() * env.rocket.getMass()) + env.rocket.getThrust() + drag + lift;
		double torque = lift.norm() * env.rocket.getCopOffset() * sgn(lift.cross(env.rocket.getHeading()));

		// accelerate linear velocity
		env.rocket.velocity = (force / env.rocket.getMass()) * env.getSimulationStepTimeInSeconds();
		// accelerate angular velocity
		env.rocket.angular_velocity = (torque / env.rocket.getInertia()) * env.getSimulationStepTimeInSeconds();
		// change position as per linear velocity
		env.rocket.position += env.rocket.velocity * env.getSimulationStepTimeInSeconds();
		// change angle as per angular velocity
		env.rocket.angle += env.rocket.angular_velocity * env.getSimulationStepTimeInSeconds();
		// if position.y is less than or equal to 0, change to landed state
		if (env.rocket.position.getY() <= 0)
		{
			env.setState<LandedPhysicsState>();
		}

		env.rocket.incrementTimeSinceIgnition(env.getSimulationStepTimeInSeconds());
	}
};

class LaunchPhysicsState: public PhysicsState
{
public:
	void onEntry(PhysicsEnvironment& env) override
	{
		// do nothing
	}

	void onStepSimulation(PhysicsEnvironment& env) override
	{
		Vector2D flow_velocity = env.getWindVelocity() - env.rocket.velocity;

		double lift_speed = flow_velocity.dot(env.rocket.getHeading().getRhsOrthogonalVector());
		Vector2D lift = env.rocket.getHeading().getRhsOrthogonalVector() * env.rocket.getFinDragCoefficient() * env.getAirDensity() *
						0.5 * lift_speed * lift_speed * env.rocket.getFinArea();

		double drag_speed = flow_velocity.dot(env.rocket.getHeading());
		Vector2D drag = env.rocket.getHeading() * env.rocket.getHeadingDragCoefficient() * env.getAirDensity() *
						0.5 * drag_speed * drag_speed * env.rocket.getHeadingArea();

		Vector2D force = (env.getGravitationalAcceleration() * env.rocket.getMass()) + env.rocket.getThrust() + drag + lift;

		// accelerate linear velocity
		env.rocket.velocity = (force / env.rocket.getMass()) * env.getSimulationStepTimeInSeconds();
		// set angular velocity to 0
		env.rocket.angular_velocity = 0;
		// set velocity to projection of velocity along launch rail vector
		env.rocket.velocity = env.getLaunchRail().normalized() * env.rocket.velocity.dot(env.getLaunchRail().normalized());
		// change position as per linear velocity
		env.rocket.position += env.rocket.velocity * env.getSimulationStepTimeInSeconds();
		// set angle to launch rail angle
		env.rocket.angle = atan2(env.getLaunchRail().getY(), env.getLaunchRail().getX());
		// if position projected along launch rail vector exceeds launch rail magnitude, change to flight state
		if (env.rocket.position.dot(env.getLaunchRail().normalized()) > env.getLaunchRail().norm())
		{
			env.setState<FlightPhysicsState>();
		}

		env.rocket.incrementTimeSinceIgnition(env.getSimulationStepTimeInSeconds());
	}
};

int main(int argc, char** argv)
{
	if (argc > 2)
	{
		throw std::runtime_error("Too many arguments.");
	}
	else if (argc > 1)
	{
		if (std::string(argv[1]) == "-t" || std::string(argv[1]) == "--tests")
		{
			RUN_ALL_TESTS();
			return 0;
		}
		else
		{
			throw std::runtime_error("Invalid argument.");
		}
	}

	RocketBuilder builder;
	builder.setMass(0.2);
	builder.setRadius(0.005);
	builder.setLength(0.020);
	builder.setBurnTime(5);
	builder.setRecoveryDelay(3);
	builder.setAverageThrust(50);
	builder.setCopOffset(0.005);
	builder.setNoseConeDragCoefficient(0.5);
	builder.setParachuteDragCoefficient(2.0);
	builder.setFinDragCoefficient(1.3);
	builder.setNoseConeArea(0.00007);
	builder.setParachuteArea(0.00035);
	builder.setFinArea(0.00007);

	PhysicsEnvironment env(builder, Vector2D(0.0, 1.0), Vector2D(10.0, 0.0));
	env.setState<LaunchPhysicsState>();
	env.simulate();

	return 0;
}