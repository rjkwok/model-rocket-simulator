/*
	Mathematical vector representation and manipulation

	Author : Richard Kwok
	Date : 2018/08/18
*/

#ifndef RJK_VECTOR_H
#define RJK_VECTOR_H

#include <stdlib.h>
#include <iostream>
#include <cmath>
#include "testing.h"

template<typename T>
class Vector2
{
	T x;
	T y;

public:
	Vector2()
	{
		this->setX(0.0);
		this->setY(0.0);
	}

	Vector2(T x, T y)
	{
		this->setX(x);
		this->setY(y);
	}

	T getX(void) const
	{
		return this->x;
	}

	void setX(T x)
	{
		this->x = x;
	}

	double getY(void) const
	{
		return this->y;
	}

	void setY(double y)
	{
		this->y = y;
	}

	Vector2<T> getRhsOrthogonalVector(void) const
	{
		return Vector2<T>(this->getY(), -this->getX());
	}

	T norm()
	{
		return sqrt(this->getX() * this->getX() + this->getY() * this->getY());
	}

	Vector2<T> normalized()
	{
		Vector2<T> normalized_vector = *this;
		normalized_vector /= this->norm();
		return normalized_vector;
	}

	double dot(const Vector2<T>& rhs)
	{
		return this->getX() * rhs.getX() + this->getY() * rhs.getY();
	}

	double cross(const Vector2<T>& rhs) const
	{
		return this->getX() * rhs.getY() - rhs.getX() * this->getY();
	}

	Vector2<T>& operator+=(const Vector2<T>& rhs)
	{
		this->setX(this->getX() + rhs.getX());
		this->setY(this->getY() + rhs.getY());
		return *this;
	}

	Vector2<T>& operator-=(const Vector2<T>& rhs)
	{
		this->setX(this->getX() - rhs.getX());
		this->setY(this->getY() - rhs.getY());
		return *this;
	}

	Vector2<T>& operator*=(T rhs)
	{
		this->setX(this->getX() * rhs);
		this->setY(this->getY() * rhs);
		return *this;
	}

	Vector2<T>& operator/=(T rhs)
	{
		this->setX(this->getX() / rhs);
		this->setY(this->getY() / rhs);
		return *this;
	}
};

template <typename T>
Vector2<T> operator+(Vector2<T> lhs, const Vector2<T>& rhs)
{
	lhs += rhs;
	return lhs;
}

template <typename T>
Vector2<T> operator-(Vector2<T> lhs, const Vector2<T>& rhs)
{
	lhs -= rhs;
	return lhs;
}

template <typename T>
Vector2<T> operator*(Vector2<T> lhs, double rhs)
{
	lhs *= rhs;
	return lhs;
}

template <typename T>
Vector2<T> operator/(Vector2<T> lhs, double rhs)
{
	lhs /= rhs;
	return lhs;
}

template <typename T>
bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
	return lhs.getX() == rhs.getX() && lhs.getY() == rhs.getY();
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector2<T>& obj)
{
	os << "[" << obj.getX() << ", " << obj.getY() << "]";
    return os;
}

typedef Vector2<double> Vector2D;

TEST(VectorAddTest)
{
	Vector2D vector_a(2.0, 3.0);
	vector_a += Vector2D(3.0, 4.0);

	ASSERT_EQ(vector_a, Vector2D(5.0, 7.0))
	ASSERT_EQ(Vector2D(3.0, 4.0) + Vector2D(2.0, 3.0), Vector2D(5.0, 7.0))
}

TEST(VectorSubtractTest)
{
	Vector2D vector_a(2.0, 5.0);
	vector_a -= Vector2D(3.0, 4.0);

	ASSERT_EQ(vector_a, Vector2D(-1.0, 1.0))
	ASSERT_EQ(Vector2D(3.0, 4.0) - Vector2D(2.0, 5.0), Vector2D(1.0, -1.0))
}

TEST(VectorScaleTest)
{
	Vector2D vector_a(2.0, 3.0);
	vector_a *= 2.0;

	ASSERT_EQ(vector_a, Vector2D(4.0, 6.0))
	ASSERT_EQ(Vector2D(2.0, 3.0) * 2.0, Vector2D(4.0, 6.0))

	vector_a /= 2.0;

	ASSERT_EQ(vector_a, Vector2D(2.0, 3.0))
	ASSERT_EQ(Vector2D(4.0, 6.0) / 2.0, Vector2D(2.0, 3.0))
}

TEST(VectorNormTest)
{
	ASSERT_EQ(Vector2D(3.0, 4.0).norm(), 5.0)
}

TEST(VectorNormalizeTest)
{
	ASSERT_EQ(Vector2D(3.0, 4.0).normalized(), Vector2D(0.6, 0.8))
}

TEST(VectorDotTest)
{
	ASSERT_EQ(Vector2D(3.0, 4.0).dot(Vector2D(1.0, 0.0)), 3.0)
	ASSERT_EQ(Vector2D(3.0, 4.0).dot(Vector2D(0.0, 1.0)), 4.0)
	ASSERT_EQ(Vector2D(3.0, 4.0).dot(Vector2D(0.6, 0.8)), 5.0)
}

TEST(VectorRhsOrthogonalTest)
{
	ASSERT_EQ(Vector2D(0.0, 1.0).getRhsOrthogonalVector(), Vector2D(1.0, 0.0))
}

TEST(VectorCrossTest)
{
	ASSERT_EQ(Vector2D(1, 0).cross(Vector2D(0, 1)), 1.0)
	ASSERT_EQ(Vector2D(0, 1).cross(Vector2D(1, 0)), -1.0)
}

#endif