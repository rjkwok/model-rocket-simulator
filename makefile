CC = g++
FLAGS = -std=c++11
SOURCES = main.cpp
INCLUDES = -Iunit-test-framework
OUTPUT = main.exe

all:
	$(CC) $(FLAGS) $(INCLUDES) $(SOURCES) -o $(OUTPUT)